
const vscode = require('vscode')
const SqliteApi = require('./sqliteapi').SqliteApi

let sqliteApi = new SqliteApi("tasks.db")

function doAssign() {

    let doc = vscode.window.activeTextEditor.document
    for(let i=0; i<doc.lineCount; i++) {
        let line = doc.lineAt(i).text
        if (line!="") {
            console.log("New AR:", line)

            const status = 0
            const category = ""
            const description = line
            const date = new Date().toLocaleDateString()
            const time = new Date().toLocaleTimeString()
  
            sqliteApi.createTask(status, category, description, date, time)
        }
    }

    vscode.window.showInformationMessage('Assign the new notes from the current editor...')
}

const STATUS = ["New", "InProgress", "Done", "Obsolete", "Rejected"]

function doChangeStatus(inc) {

    let editor = vscode.window.activeTextEditor
    if (!editor) {
        return
    }

    let cur = editor.selection.active.line
    const curLine = editor.document.lineAt(cur);

    let partsExpr = /(?:[^\s"]+|"[^"]*")+/g
    let match = partsExpr.exec(curLine.text)
    match = partsExpr.exec(curLine.text)
    let stStart = match.index
    let stEnd = stStart + match[0].length
    let status = curLine.text.slice(stStart, stEnd)
    let i = STATUS.indexOf(status)
    // console.log(stStart, stEnd, status, i)
    if (i>=0) {
        i = (i+inc)
        while (i<0) { i = i + STATUS.length }
        i = i % STATUS.length
        let newStatus = STATUS[i]
        const edit = new vscode.WorkspaceEdit();
        const range = new vscode.Range(new vscode.Position(cur, stStart), new vscode.Position(cur, stEnd))
        edit.replace(editor.document.uri, range, newStatus)
        vscode.workspace.applyEdit(edit);
    }
}

function doDecStatus() { doChangeStatus(-1) }
function doIncStatus() { doChangeStatus(+1) }

function addCommand(context, id, text, handler) {
    let cmd = vscode.commands.registerCommand(id, handler)
    let cmdSB = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 1000)
    cmdSB.command = id
    cmdSB.text = text
    cmdSB.show()

    context.subscriptions.push(cmd)
    context.subscriptions.push(cmdSB)
}

function activate(context) {

    console.log('Congratulations, your extension "todo-ext" is now active!')

    addCommand(context, 'todo-ext.assign', 'Assign', doAssign)
    addCommand(context, 'todo-ext.decStatus', '<< Status', doDecStatus)
    addCommand(context, 'todo-ext.incStatus', 'Status >>', doIncStatus)
}

function deactivate() {}

module.exports = {
    activate,
    deactivate
}
