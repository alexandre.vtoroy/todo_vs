
const sqlite3 = require('sqlite3').verbose();

class SqliteApi {

    db = null

    constructor(dbaseName) {
        // console.log("DB constructed", dbaseName)
        // console.log('Current directory:', process.cwd())
        this.db = new sqlite3.Database(dbaseName)
        const st = this.db.prepare('CREATE TABLE IF NOT EXISTS tasks (id INTEGER NOT NULL PRIMARY KEY, status INTEGER, category TEXT, description TEXT, date DATE, time TIME, comment TEXT)')
        st.run()
    }

    createTask(status, category, description, date, time) {
        // console.log("create task", description)
        const st = this.db.prepare('INSERT INTO tasks VALUES (NULL, ?, ?, ?, ?, ?, ?)').run(status, category, description, date, time, '')
        st.run()
    }

    getTasks(query) {
    }
}

module.exports = {
    SqliteApi: SqliteApi
}
